# SubRepo

Dies ist das Submodul des `HauptRepo`-Beispielprojekts. Es dient dazu, das Konzept und die Verwaltung von Git-Submodulen in GitLab zu demonstrieren.

## Hinweis

`SubRepo` ist eigenständig, kann aber auch als Teil des `HauptRepo` betrachtet werden. Änderungen in diesem Repository sollten im Hauptrepository aktualisiert werden, um den neuesten Stand des Submoduls widerzuspiegeln.
